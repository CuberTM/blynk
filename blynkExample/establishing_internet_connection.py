import network
from machine import Pin
from time import sleep

blinkLED = Pin(2,Pin.OUT)
WIFI_SSID = 'TypeHereWifiSSID'
WIFI_PASS = 'TypeHereWifiPassword'

def wlanConnect(WIFI_SSID,WIFI_PASS):
    stationInterface = network.WLAN(network.STA_IF)
    stationInterface.active(True)
    stationInterface.connect(WIFI_SSID,WIFI_PASS)
    sleep(15)
    wlanStatus = stationInterface.isconnected()
    return wlanStatus

def wlanStatusMessage(wlanStatus,blinkLED):
    if wlanStatus==True:
        print("Status: Connected to wifi network")
    else:
        print("Status: Wifi connection FAILED")
        for i in range(20):
            blinkLED.value(not blinkLED.value())
            sleep(0.6)      

wlanStatus = wlanConnect(WIFI_SSID,WIFI_PASS)
wlanStatusMessage(wlanStatus,blinkLED)
