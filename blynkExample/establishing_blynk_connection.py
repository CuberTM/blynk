import blynklib
import network
from machine import Pin
from time import sleep

BLYNK_AUTH = 'na8LpcLiiPVNn7G3qo9BLT1LZB5AgJ9j'
SERVER_IP = '192.168.0.1'
SERVER_PORT = 8080

blinkLED = Pin(2,Pin.OUT)
WIFI_SSID = 'TypeHereWifiSSID'
WIFI_PASS = 'TypeHereWifiPassword'

stationInterface = network.WLAN(network.STA_IF)
stationInterface.active(True)
stationInterface.connect(WIFI_SSID,WIFI_PASS)
sleep(15)
wlanStatus = stationInterface.isconnected()

if wlanStatus==True:
    print("Status: Connected to wifi network")
else:
    print("Status: Wifi connection FAILED")
    for i in range(20):
        blinkLED.value(not blinkLED.value())
        sleep(0.6) 

print("Connecting to Blynk...")
blynkAct = blynklib.Blynk(BLYNK_AUTH, server=SERVER_IP, port=SERVER_PORT)

while True:
    blynkAct.run()
