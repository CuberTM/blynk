import blynklib
import random
import network
from machine import Pin
from time import sleep

BLYNK_AUTH = 'na8LpcLiiPVNn7G3qo9BLT1LZB5AgJ9j'
SERVER_IP = '192.168.0.1'
SERVER_PORT = 8080

blinkLED = Pin(2,Pin.OUT)
WIFI_SSID = 'TypeHereWifiSSID'
WIFI_PASS = 'TypeHereWifiPassword'

pinV2 = 2
pinV3 = 3
time = 2

wlanConnect(WIFI_SSID,WIFI_PASS):
stationInterface = network.WLAN(network.STA_IF)
stationInterface.active(True)
stationInterface.connect(WIFI_SSID,WIFI_PASS)
sleep(15)
wlanStatus = stationInterface.isconnected()

if wlanStatus==True:
    print("Status: Connected to wifi network")
else:
    print("Status: Wifi connection FAILED")
    for i in range(20):
        blinkLED.value(not blinkLED.value())
        sleep(0.6)  

blynkAct = blynklib.Blynk(BLYNK_AUTH, server=SERVER_IP, port=SERVER_PORT)

@blynkAct.handle_event('read V2')
def sendDataTo(pinV2):
    randomData= random.randint(0, 50)
    blynkAct.virtual_write(pinV2, randomData)
    return
   
@blynkAct.handle_event('read V3')
def sendAnotherDataTo(pinV3):    
    randomData= random.randint(0, 50)
    blynkAct.virtual_write(pinV3, randomData)
    return

while True:
    blynkAct.run()
